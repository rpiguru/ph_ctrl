import csv
import logging
import sqlite3 as lite


def save_report_file():
    db_name = "/home/pi/ph_ctrl/db.sqlite"
    conn = None
    try:
        conn = lite.connect(db_name)
        curs = conn.cursor()
        curs.execute("SELECT * FROM tb_sensor")
        rows = curs.fetchall()
        conn.close()

        re_list = [['No.', 'pH Value', 'Date & Time']]
        if rows:
            for i in range(len(rows)):
                tmp = list(rows[i])
                re_list.append(tmp)

        file_name = '/home/pi/ph_ctrl/data.csv'

        b = open(file_name, 'wb')
        a = csv.writer(b)
        a.writerows(re_list)
        b.close()
        return file_name

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Error %s:" % e.args[0])
        return None

print save_report_file()
