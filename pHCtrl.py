# Main controller of pH reading system
"""

**** pH Calibration.

    pH(correct) = pH(captured) + ZP + (T - 25) * 0.05916 * S / 100

    ZP(Zero point) : pH value at which the total output electrode voltage is equal to 0 mV.
    S(Slope): the amount of changes when temperature is changed 1 degree from the 25'C.
         The typical amount is 0.05916 and this Slope is expressed as percentage of this value.
         e.g : if changed amount is 0.06, Slope is 101(%)

    T : Temperature of the liquid.


    Calibration Formula

    Let's assume that we are performing 2-point calibration with pH7, pH4 buffers

    pH_7 = 7.0 + (T_7 - 25) * 0.05916 * S / 100 + ZP
    pH_4 = 4.0 + (T_4 - 25) * 0.05916 * S / 100 + ZP

        where   pH_7 : detected value when probe is placed in pH7 buffer
                pH_4 : detected value when probe is placed in pH4 buffer
                T_7  : Temperature of pH7 buffer
                T_4  : Temperature of pH4 buffer

    S = (pH_7 - pH_4 - 3) / (T_7 - T_4) / 0.05916 * 100
    ZP = pH_7 - 7 - (T_7 - 25) * 0.05916 * S / 100

**** Working with O2 sensor.

    Output range of O2 sensor is 1mV ~ 20 mV when 100ohm resister is loaded.
    We are going to use LMV358 module which has 100 times amplifying function.
    So input range of ADS1015 would be 0.1V ~ 2V.

"""

import logging
import os
import sqlite3 as lite
import time
import xml.etree.ElementTree
import Adafruit_ADS1x15

current_path = "/home/pi/ph_ctrl"


class ph_Manager:

    log_level = 10
    log_file_name = ''
    db_name = ''
    ads_gain = 2/3
    rate = 60
    i2c_address = 0x48
    ph_ads_pin_num = 0
    temp_ads_pin_num = 1
    o2_ads_pin_num = 2
    adc = None
    sample_cnt = 10
    zp = 0.0
    slope = 100

    def __init__(self):
        self.db_name = self.get_param_from_xml('DB_NAME')

        self.update_params()
        self.check_table()

    def update_params(self):
        """
        update parameters from configuration file
        :return:
        """
        self.rate = int(self.get_param_from_xml('READING_RATE'))
        self.ads_gain = int(self.get_param_from_xml('ADS_GAIN'))
        self.ph_ads_pin_num = int(self.get_param_from_xml('pH_ADS_PIN_NUM'))
        self.temp_ads_pin_num = int(self.get_param_from_xml('TEMP_ADS_PIN_NUM'))
        self.o2_ads_pin_num = int(self.get_param_from_xml('O2_ADS_PIN_NUM'))

        self.sample_cnt = int(self.get_param_from_xml('SAMPLE_COUNT'))
        self.zp = float(self.get_param_from_xml('pH_ZP'))
        self.slope = float(self.get_param_from_xml('pH_SLOPE'))

        tmp = self.get_param_from_xml('I2C_ADDRESS')
        self.i2c_address = int(tmp, 16)
        self.adc = Adafruit_ADS1x15.ADS1015(address=self.i2c_address)

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(current_path + '/config.xml').getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break
        return tmp

    def check_table(self):
        """
        Check whether data table exists or not
        If not exists, create it.
        Column : No. id, val, timestamp
        """

        conn = None
        try:
            conn = lite.connect(self.db_name)
            os.system('chmod 777 ' + self.db_name)     # change permission of db file
            curs = conn.cursor()
            sql = 'create table if not exists tb_sensor ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'ph_val NUMERIC , ' \
                  'o2_val NUMERIC , ' \
                  'timestamp DATETIME, ' \
                  'temp NUMERIC ' \
                  ');'
            curs.execute(sql)
            conn.commit()
            conn.close()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Creating Table failed...")
            return False
        finally:
            if conn:
                conn.close()

    def get_ph_val(self):
        val_list = []
        for i in range(self.sample_cnt):
            val = self.adc.read_adc(self.ph_ads_pin_num, gain=self.ads_gain)
            voltage = 6.144 * val / 2048.0
            ph_val = 3.5 * voltage
            val_list.append(ph_val)
            time.sleep(0.1)

        # remove max/min value from the list
        val_list.sort()
        val_list.pop(0)
        val_list.pop(-1)

        # get average value in list
        avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
        return avg_val

    def get_temperature(self):
        val_list = []
        for i in range(self.sample_cnt):
            val = self.adc.read_adc(self.temp_ads_pin_num, gain=self.ads_gain)

            voltage = 6.144 * val / 2048.0
            temp = 25 + (voltage - 0.75) * 10

            val_list.append(temp)
            time.sleep(0.1)
        # remove max/min value from the list
        val_list.sort()
        val_list.pop(0)
        val_list.pop(-1)

        # get average value in list
        avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
        return avg_val

    def get_o2_val(self):
        val_list = []
        for i in range(self.sample_cnt):
            val = self.adc.read_adc(self.o2_ads_pin_num, gain=self.ads_gain)

            voltage = 6.144 * val / 2048.0
            tmp = 25 + (voltage - 0.75) * 10

            val_list.append(tmp / 10.0)
            time.sleep(0.1)
        # remove max/min value from the list
        val_list.sort()
        val_list.pop(0)
        val_list.pop(-1)

        # get average value in list
        avg_val = reduce(lambda x, y: x + y, val_list) / len(val_list)
        return avg_val

    def run(self):
        """
        Get pH value and temperature and perform calibration.
        pH(correct) = pH(captured) + ZP + (T - 25) * 0.05916 * S / 100
        """
        ph_val = self.get_ph_val()
        tmp_val = self.get_temperature()
        o2_val = self.get_o2_val()
        # TODO: get sensor value from the o2_val - current value, not percentage of Oxygen...

        # calibration
        cur_ph_val = ph_val + self.zp + (tmp_val - 25) * 0.05916 * self.slope / 100.0

        return self.upload_to_table((cur_ph_val, tmp_val, o2_val))

    @staticmethod
    def upload_to_table(data):
        conn = None
        try:
            conn = lite.connect(ph_ctrl.db_name)
            os.system('chmod 777 ' + ph_ctrl.db_name)     # change permission of db file
            curs = conn.cursor()
            sql = "INSERT INTO tb_sensor (ph_val, timestamp, temp, o2_val) values(" + ("%.2f" % data[0]) + \
                  ", datetime('now', 'localtime'), " + ("%.2f" % data[1]) + ", " + ("%.2f" % data[2]) + ");"
            print sql
            curs.execute(sql)
            conn.commit()
            conn.close()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            logging.error("Failed to insert data ...")
            return False
        finally:
            if conn:
                conn.close()

if __name__ == '__main__':

    print "Initializing Sensor..."
    ph_ctrl = ph_Manager()

    # initialize logging
    log_file_name = ph_ctrl.get_param_from_xml('LOG_FILE')
    log_level = int(ph_ctrl.get_param_from_xml('LOG_LEVEL'))
    logging.basicConfig(level=log_level, filename=log_file_name,
                        format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    while True:
        ph_ctrl.update_params()
        start_time = time.time()
        ph_ctrl.run()

        while time.time() - start_time < ph_ctrl.rate:
            time.sleep(0.2)
